const puppeteer = require("puppeteer");
const URL = process.env.URL || "http://localhost:3000";

(async () => {
  const browser = await puppeteer.launch({ headless: true });
  const page = await browser.newPage();

  await page.coverage.startCSSCoverage();
  await page.goto(URL, { waitUntil: "load" }); // domcontentload, load, networkidle0

  // await autoScroll(page);

  // await page.click("#darkTheme");

  const cssCoverage = await page.coverage.stopCSSCoverage();

  let criticalCSS = "";
  for (const entry of cssCoverage) {
    for (const range of entry.ranges) {
      criticalCSS += entry.text.slice(range.start, range.end) + "\n";
    }
  }

  console.log(criticalCSS);

  await page.close();
  await browser.close();
})();

/* async function autoScroll(page) {
  await page.evaluate(async () => {
    await new Promise((resolve, reject) => {
      var totalHeight = 0;
      var distance = 100;
      var timer = setInterval(() => {
        var scrollHeight = document.body.scrollHeight;
        window.scrollBy(0, distance);
        totalHeight += distance;

        if (totalHeight >= scrollHeight) {
          clearInterval(timer);
          resolve();
        }
      }, 100);
    });
  });
} */
